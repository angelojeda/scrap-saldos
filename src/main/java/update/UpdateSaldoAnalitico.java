package update;

import process.Analitico2Final;
import pojo.Saldo;
import pojo.SaldoAnalitico;
import pojo.SaldoFinal;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by Angel on 12/04/2017.
 */
public class UpdateSaldoAnalitico {

	private static final Logger logger = Logger.getLogger(UpdateSaldoAnalitico.class);
	private String saldosAnaliticosURL;
	private int response;

	public List<SaldoAnalitico> update(List<SaldoAnalitico> saldoAnaliticos) {

		SaldoFinal saldoFinal = new SaldoFinal();

		Double sumaLimite = 0.0;
		Double sumaDisponible = 0.0;
		Double sumaSaldo = 0.0;
		Double sumaMoratorios = 0.0;
		Double sumaTotal = 0.0;

		for (SaldoAnalitico saldoAnalitico : saldoAnaliticos) {
			sumaLimite = sumaLimite + saldoAnalitico.getLimiteCredito();
			sumaDisponible = sumaDisponible + saldoAnalitico.getSaldoCredito();

			if (saldoAnalitico.getFormaPago().equals("CREDITO")) {
				sumaSaldo = sumaSaldo + saldoAnalitico.getSaldoDetallados().get(1).getSaldoCliente();
				sumaMoratorios = sumaMoratorios
						+ saldoAnalitico.getSaldoDetallados().get(1).getInteresesMoratoriosCliente();
				sumaTotal = sumaTotal + saldoAnalitico.getSaldoDetallados().get(1).getTotalCliente();
			}
		}

		saldoFinal.setSumaLimite(Math.round(sumaLimite));
		saldoFinal.setSumaDisponible(Math.round(sumaDisponible));
		saldoFinal.setSumaSaldo(Math.round(sumaSaldo));
		saldoFinal.setSumaMoratorios(Math.round(sumaMoratorios));
		saldoFinal.setSumaTotal(Math.round(sumaTotal));
		saldoFinal.setSaldosAnaliticos(new Analitico2Final().convert(saldoAnaliticos));

		for (Saldo saldo : saldoFinal.getSaldosAnaliticos()) {

			response = new Update().send(saldosAnaliticosURL, wsBody(saldo));

			if (response == 201) {
				logger.info("Se actualizo el saldo analitico - " + saldo.getCliente());
			} else {
				logger.error("Error (" + response + ") No se pudo actualizar - " + saldo.getCliente());
			}
		}

		// new UpdateDetalleEstacion().start(saldoAnaliticos); //TODO remove from here

		return saldoAnaliticos;
	}

	private String wsBody(Saldo saldo) {

		String bodySaldosAnaliticos = "gasolinera=" + saldo.getGasolinera() + "&cliente=" + saldo.getCliente()
				+ "&encargado_cobro=" + saldo.getEncargadoCobro() + "&forma_pago=" + saldo.getFormaPago()
				+ "&estado_suspension=" + saldo.getEstadoSuspencion() + "&limite_credito=" + saldo.getLimiteCredito()
				+ "&disponible_contado=" + saldo.getDisponibleContado() + "&disponible_credito="
				+ saldo.getDisponibleCredito() + "&saldo_contado=" + saldo.getSaldoContado() + "&comprometido_contado="
				+ saldo.getComprometidoContado() + "&moratorios_contado=" + saldo.getMoratoriosContado()
				+ "&total_contado=" + saldo.getTotalContado() + "&comprometido_contado_pemex="
				+ saldo.getComprometidoContadoPemex() + "&total_contado_pemex=" + saldo.getTotalContadoPemex()
				+ "&saldo_credito=" + saldo.getSaldoCredito() + "&comprometido_credito="
				+ saldo.getComprometidoContado() + "&moratorios_credito=" + saldo.getMoratoriosCredito()
				+ "&total_credito=" + saldo.getTotalCredito() + "&fecha=" + saldo.getFecha();

		return bodySaldosAnaliticos;
	}// TODO change this to send as Object or JSON

	public String getSaldosAnaliticosURL() {
		return saldosAnaliticosURL;
	}

	public void setSaldosAnaliticosURL(String saldosAnaliticosURL) {
		this.saldosAnaliticosURL = saldosAnaliticosURL;
	}
}

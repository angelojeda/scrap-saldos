package process;

import pojo.Chofer;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angel on 11/05/2017.
 */
public class Choferes {

	private List<Chofer> choferes = new ArrayList<Chofer>();

	public List<Chofer> init(List<String> choferesString) {

		for (String choferString : choferesString) {
			Chofer chofer = new Chofer();

			chofer.setId(Integer.parseInt(choferString.split("-")[0].trim()));
			chofer.setNombre(choferString.split("-")[1].trim().split(",")[0].split(" ")[1]);
			chofer.setApellido(choferString.split("-")[1].trim().split(",")[0].split(" ")[0]);
			chofer.setNumEcon(Integer.parseInt(choferString.split("-")[1].trim().split(",")[1].trim()));
			getChoferes().add(chofer);
		}

		return getChoferes();
	}

	public List<Chofer> getChoferes() {
		return choferes;
	}

	public void setChoferes(List<Chofer> choferes) {
		this.choferes = choferes;
	}
}
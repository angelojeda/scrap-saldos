package process;

import pojo.Chofer;
import file.DataReader;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by Angel on 26/05/2017.
 */
public class FindChofer {

	private static final Logger logger = Logger.getLogger(FindChofer.class);

	public int find(String choferString, List<Chofer> choferes) {

		int choferId = 0;

		String[] nombre = choferString.split(" ");

		for (Chofer chofer : choferes) {

			if (chofer.getApellido().contains("�")) {
				chofer.setApellido(chofer.getApellido().replace("�", "n"));

			}

			if (chofer.getApellido().toUpperCase().equals(nombre[0])
					&& (chofer.getNombre().toUpperCase().equals(nombre[1])
							|| chofer.getNombre().toUpperCase().equals(nombre[2]))) {

				choferId = chofer.getId();
				break;
			} else {
				// log.info("No se encontro "+ choferString+ " en la lista de choferes");
			}

		}

		return choferId;
	}

}

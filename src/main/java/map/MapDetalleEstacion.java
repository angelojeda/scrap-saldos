package map;

import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import process.ParseCurrencyNumber;
import pojo.DetalleEstacion;
import pojo.SaldoAnalitico;
import process.ConnectURL;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Angel on 18/04/2017.
 */
public class MapDetalleEstacion {

	private final String DETALLE_ESTACION_PATH = "/portal/sccoi003/controlador?Destino=sccoi003_01.jsp";
	private final String INI_DATA = "fini";
	private final String FIN_DATA = "ffin";
	private final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";
	private final int DAYS_BEFORE_CURRENT = -6;

	public SaldoAnalitico startMap(SaldoAnalitico saldoAnalitico, Map<String, String> cookies) {

		Document d = new ConnectURL().getDataSubmit(cookies, getMapData(), DETALLE_ESTACION_PATH);
		saldoAnalitico.setDetalleEstacionList(getDetalles(d.getElementById("maincontent").select("tr[class^=color]")));

		return saldoAnalitico;
	}

	private List<DetalleEstacion> getDetalles(Elements rows) {

		List<DetalleEstacion> detalleEstaciones = new ArrayList();

		for (Element item : rows) {

			DetalleEstacion detalleEstacion = new DetalleEstacion();

			detalleEstacion.setDocumentoCargo(item.child(0).childNode(0).toString());
			detalleEstacion.setImporteCargo(new ParseCurrencyNumber().parse(item.child(1).childNode(0).toString()));
			detalleEstacion.setFormaPago(item.child(2).childNode(0).toString());
			detalleEstacion.setMoneda(item.child(3).childNode(0).toString());
			detalleEstacion.setFechaVencimiento(item.child(4).childNode(0).toString());
			detalleEstacion.setTipo(item.child(5).childNode(0).toString());
			detalleEstacion.setBanco(item.child(6).childNode(0).toString());
			detalleEstacion.setDocumentoPago(item.child(7).childNode(0).toString());
			detalleEstacion.setFechaPago(item.child(8).childNode(0).toString());
			detalleEstacion.setImportePago(new ParseCurrencyNumber().parse(item.child(9).childNode(0).toString()));
			detalleEstacion
					.setInteresesMoratorios(new ParseCurrencyNumber().parse(item.child(10).childNode(0).toString()));
			detalleEstacion
					.setIvaInteresesMoratorios(new ParseCurrencyNumber().parse(item.child(11).childNode(0).toString()));

			detalleEstaciones.add(detalleEstacion);

		}

		return detalleEstaciones;
	}

	private Map<String, String> getMapData() {

		Map<String, String> data = new HashMap();

		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		DateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		calendar.add(Calendar.DATE, DAYS_BEFORE_CURRENT);

		Date oldDate = calendar.getTime();

		data.put(INI_DATA, format.format(oldDate));
		data.put(FIN_DATA, format.format(currentDate));

		return data;
	}

}

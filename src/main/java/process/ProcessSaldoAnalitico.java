package process;

import pojo.Chofer;
import pojo.Estacion;
import pojo.SaldoAnalitico;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.log4j.LogManager;
import org.apache.log4j.Logger;

/**
 * Created by Angel on 04/04/2017.
 */
public class ProcessSaldoAnalitico {

	// private ConnectURL conection;
	private List<String> loginList = new ArrayList();
	private static final Logger logger = LogManager.getLogger(ProcessSaldoAnalitico.class);

	public List<SaldoAnalitico> start(List<Estacion> estaciones, List<Chofer> choferes) {

		List<SaldoAnalitico> saldosAnaliticos;

		saldosAnaliticos = getSaldosAnaliticosList(estaciones, choferes);

		return saldosAnaliticos;
	}

	private static SaldoAnalitico scrapSaldoWeb(Estacion estacionData, List<Chofer> choferes) {

		SaldoAnalitico saldoAnalitico = new DataScrap().scrapDataFromWeb(getWebCookies(estacionData), choferes,
				estacionData);

		saldoAnalitico.setGasolinera(Integer.parseInt(estacionData.getIdEstacion()));

		return saldoAnalitico;
	}

	private static Map<String, String> getWebCookies(Estacion estacionData) {

		Map<String, String> cookies = null;
		while (cookies == null) {
			cookies = new ConnectURL().connect(estacionData.getUser(), estacionData.getPass());
		}

		// Map<String, String> cookies = new
		// ConnectURL().connect(estacionData.getUser(), estacionData.getPass());

		return cookies;
	}

	private static List<SaldoAnalitico> getSaldosAnaliticosList(List<Estacion> estaciones, List<Chofer> choferes) {

		logger.info("Se encontraron " + estaciones.size() + " estaciones");
		logger.info("Se encotnraron " + choferes.size() + " hoferes");

		List<SaldoAnalitico> saldosAnaliticos = new ArrayList();

		for (Estacion estacion : estaciones) {

			logger.info("Se comenzo a procesar - " + estacion.getNombreEstacion());
			saldosAnaliticos.add(scrapSaldoWeb(estacion, choferes));
			logger.info("Se proceso - " + estacion.getNombreEstacion());

		}

		return saldosAnaliticos;

	}

}

package map;

import process.ParseCurrencyNumber;
import pojo.SaldoAnalitico;
import pojo.SaldoDetallado;
import org.jsoup.nodes.Element;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angel on 23/03/2017.
 */
public class MapSaldoDetallado {

	private List<SaldoDetallado> saldosDetallados = new ArrayList<SaldoDetallado>();

	public SaldoAnalitico mapData(SaldoAnalitico saldo, Element element) {

		saldosDetallados.add(mapSaldoData(new SaldoDetallado(), element, "tr.color_non > td"));

		if (!(element.select("tr.color_par > td").size() == 0)) {
			saldosDetallados.add(mapSaldoData(new SaldoDetallado(), element, "tr.color_par > td"));
		}

		saldo.setSaldoDetallados(saldosDetallados);

		return saldo;
	}

	private SaldoDetallado mapSaldoData(SaldoDetallado detallado, Element element, String query) {

		detallado.setTipoCuenta(element.select(query).get(0).childNodes().get(0).toString().trim());
		detallado.setMoneda(element.select(query).get(1).childNodes().get(0).toString().trim());

		if (element.select(query).get(2).childNodes().size() < 3) {
			detallado.setSaldoCliente(new ParseCurrencyNumber()
					.parse(element.select(query).get(2).childNodes().get(0).toString().trim()));
		} else {
			detallado.setSaldoCliente(new ParseCurrencyNumber()
					.parse(element.select(query).get(2).childNodes().get(1).childNodes().get(0).toString().trim()));
		}

		detallado.setComprometidoCliente(
				new ParseCurrencyNumber().parse(element.select(query).get(3).childNodes().get(0).toString().trim()));
		detallado.setInteresesMoratoriosCliente(
				new ParseCurrencyNumber().parse(element.select(query).get(4).childNodes().get(0).toString().trim()));
		detallado.setTotalCliente(
				new ParseCurrencyNumber().parse(element.select(query).get(5).childNodes().get(0).toString().trim()));

		if (!(element.select(query).get(7).childNodes().size() == 0)) {
			detallado.setSaldoPemex(new ParseCurrencyNumber()
					.parse(element.select(query).get(6).childNodes().get(0).toString().trim()));
			detallado.setComprometidoPemex(new ParseCurrencyNumber()
					.parse(element.select(query).get(7).childNodes().get(0).toString().trim()));
			detallado.setTotalPemex(new ParseCurrencyNumber()
					.parse(element.select(query).get(8).childNodes().get(0).toString().trim()));
		}

		return detallado;

	}

}

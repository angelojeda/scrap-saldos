package update;

import org.apache.log4j.Logger;
import com.mashape.unirest.http.HttpResponse;
import com.mashape.unirest.http.Unirest;
import com.mashape.unirest.http.exceptions.UnirestException;

/**
 * Created by Angel on 03/05/2017.
 */
public class Update {
	private static final Logger logger = Logger.getLogger(Update.class);

	public int send(String url, String body) {

		int responseWs = 0;

		try {
			HttpResponse<String> response = Unirest.post(url)
					.header("content-type", "application/x-www-form-urlencoded").header("cache-control", "no-cache")
					.body(body).asString();

			responseWs = handleResponse(response.getBody());

		} catch (UnirestException e) {
			e.printStackTrace();
		}

		return responseWs;
	}

	public void clear(String url, String body) {

		try {
			HttpResponse<String> response = Unirest.put(url).header("content-type", "application/x-www-form-urlencoded")
					.header("cache-control", "no-cache").body(body).asString();

		} catch (UnirestException e) {
			e.printStackTrace();
		}
	}

	private int handleResponse(String response) {

		int responseNum = 0;

		if (response.contains("201")) {
			responseNum = 201;
		} else if (response.contains("406")) {
			responseNum = 406;
		} else if (response.contains("500")) {
			responseNum = 500;
		}

		return responseNum;

	}

}

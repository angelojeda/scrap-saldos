package process;

import map.MapTrafico;
import map.*;
import pojo.Chofer;
import pojo.Estacion;
import pojo.SaldoAnalitico;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Angel on 18/03/2017.
 */
public class DataScrap {

	private Map<String, String> dateCookies;

	private Map<String, String> loginCookies;

	private static final String SALDO_ANALITICO_PATH = "/portal/sccoi001/controlador?Destino=sccoi001_01.jsp";

	private static final String CONTENIDO_PRINCIPAL = "maincontent";

	private SaldoAnalitico saldoAnalitico = new SaldoAnalitico();

	public SaldoAnalitico scrapDataFromWeb(Map<String, String> cookies, List<Chofer> choferes, Estacion estacionData) {

		saldoAnalitico.setGasolinera(Integer.parseInt(estacionData.getIdEstacion()));

		Document document = new ConnectURL().getDocument(SALDO_ANALITICO_PATH, cookies);
		new MapSaldoAnalitico().mapData(saldoAnalitico, getMainData(document), getTimestamp());
		new MapSaldoDetallado().mapData(saldoAnalitico, getMainData(document));
		new MapDetalleEstacion().startMap(saldoAnalitico, cookies);
		new MapTipoDocumento().mapData(saldoAnalitico, cookies);
		new MapRemisionesProducto().mapData(saldoAnalitico, cookies);
		new MapTrafico().startMap(saldoAnalitico, cookies, choferes);

		return saldoAnalitico;

	}

	private Element getMainData(Document document) {

		Element element = document.getElementById(CONTENIDO_PRINCIPAL);

		return element;
	}

	private String getTimestamp(String dateString) {

		String dateStr = "Martes, 4 de abril del 2017    12:08";

		DateFormat readFormat = new SimpleDateFormat("EEE',' d 'de' MMM 'del' yyyy HH:mm", new Locale("es", "ES"));

		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = null;
		try {
			date = readFormat.parse(dateString);
		} catch (ParseException e) {
			e.printStackTrace();
		}

		String formattedDate = "";
		if (date != null) {
			formattedDate = writeFormat.format(date);

		}
		return formattedDate;
	}

	private String getTimestamp() {

		DateFormat dateFormat = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		Date date = new Date();

		return dateFormat.format(date);
	}

	public Map<String, String> getDateCookies() {
		return dateCookies;
	}

	public void setDateCookies(Map<String, String> dateCookies) {
		this.dateCookies = dateCookies;
	}

	public Map<String, String> getLoginCookies() {
		return loginCookies;
	}

	public void setLoginCookies(Map<String, String> loginCookies) {
		this.loginCookies = loginCookies;
	}

}

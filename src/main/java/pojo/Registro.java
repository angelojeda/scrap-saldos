package pojo;

/**
 * Created by Angel on 16/03/2017.
 */

public class Registro {
	private String conceptoCargo;
	private String importeUnitario;
	private String importeCargoCredito;

	public String getConceptoCargo() {
		return conceptoCargo;
	}

	public void setConceptoCargo(String conceptoCargo) {
		this.conceptoCargo = conceptoCargo;
	}

	public String getImporteUnitario() {
		return importeUnitario;
	}

	public void setImporteUnitario(String importeUnitario) {
		this.importeUnitario = importeUnitario;
	}

	public String getImporteCargoCredito() {
		return importeCargoCredito;
	}

	public void setImporteCargoCredito(String importeCargoCredito) {
		this.importeCargoCredito = importeCargoCredito;
	}
}

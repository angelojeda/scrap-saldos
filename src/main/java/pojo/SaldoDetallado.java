package pojo;

import java.util.List;

/**
 * Created by Angel on 16/03/2017.
 */

public class SaldoDetallado {
	private String tipoCuenta;
	private String moneda;
	private Double saldoCliente;
	private Double comprometidoCliente;
	private Double interesesMoratoriosCliente;
	private Double totalCliente;
	private Double saldoPemex;
	private Double comprometidoPemex;
	private Double totalPemex;
	private List<TipoDocumento> tipoDocumentos;

	public String getTipoCuenta() {
		return tipoCuenta;
	}

	public void setTipoCuenta(String tipoCuenta) {
		this.tipoCuenta = tipoCuenta;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public List<TipoDocumento> getTipoDocumentos() {
		return tipoDocumentos;
	}

	public void setTipoDocumentos(List<TipoDocumento> tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	public Double getSaldoCliente() {
		return saldoCliente;
	}

	public void setSaldoCliente(Double saldoCliente) {
		this.saldoCliente = saldoCliente;
	}

	public Double getComprometidoCliente() {
		return comprometidoCliente;
	}

	public void setComprometidoCliente(Double comprometidoCliente) {
		this.comprometidoCliente = comprometidoCliente;
	}

	public Double getInteresesMoratoriosCliente() {
		return interesesMoratoriosCliente;
	}

	public void setInteresesMoratoriosCliente(Double interesesMoratoriosCliente) {
		this.interesesMoratoriosCliente = interesesMoratoriosCliente;
	}

	public Double getTotalCliente() {
		return totalCliente;
	}

	public void setTotalCliente(Double totalCliente) {
		this.totalCliente = totalCliente;
	}

	public Double getSaldoPemex() {
		return saldoPemex;
	}

	public void setSaldoPemex(Double saldoPemex) {
		this.saldoPemex = saldoPemex;
	}

	public Double getComprometidoPemex() {
		return comprometidoPemex;
	}

	public void setComprometidoPemex(Double comprometidoPemex) {
		this.comprometidoPemex = comprometidoPemex;
	}

	public Double getTotalPemex() {
		return totalPemex;
	}

	public void setTotalPemex(Double totalPemex) {
		this.totalPemex = totalPemex;
	}
}

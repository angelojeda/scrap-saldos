package map;

import process.FindChofer;
import pojo.Chofer;
import pojo.SaldoAnalitico;
import pojo.Trafico;
import process.ConnectURL;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.*;

/**
 * Created by Angel on 25/04/2017.
 */
public class MapTrafico {

	private final String INI_DATA = "fini";
	private final String FIN_DATA = "ffin";
	private final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";
	private final int DAYS_BEFORE_CURRENT = -6;
	private String DETALLE_ESTACION_PATH = "/portal/scfai001/controlador?Destino=scfai001_01.jsp";
	private String USER_DEFAULT = "PEMEX";

	public SaldoAnalitico startMap(SaldoAnalitico saldoAnalitico, Map<String, String> cookies, List<Chofer> choferes) {

		Document d = new ConnectURL().getDataSubmit(cookies, getMapData(), DETALLE_ESTACION_PATH);
		saldoAnalitico.setTraficos(getTraficoData(d.getElementById("maincontent").select("tr[class]"), cookies,
				choferes, saldoAnalitico.getGasolinera()));

		return saldoAnalitico;
	}

	private List<Trafico> getTraficoData(Elements rows, Map<String, String> cookies, List<Chofer> choferes,
			int gasolinera) {

		List<Trafico> traficoList = new ArrayList();

		for (Element row : rows) {

			Trafico trafico = new Trafico();

			trafico.setUsuario(USER_DEFAULT);
			trafico.setManual("0");
			trafico.setEs_gasolinera(gasolinera);
			trafico.setOrigen(gasolinera);
			trafico.setDestino(gasolinera);
			trafico.setProducto(getProductoId(row.childNode(7).childNode(0).toString().trim()));
			trafico.setUnidad(formatUnidadId(row.childNode(13).childNode(0).toString().trim()));
			trafico.setCantidad(Integer.parseInt(row.childNode(15).childNode(0).toString().trim().replace(".", "")));
			trafico.setTemperatura_carga(Double.parseDouble(row.childNode(17).childNode(0).toString().trim()));
			trafico.setDescargado("0");
			// trafico.setFecha(changeDateFormat(row.childNode(9).childNode(0).toString().trim()));
			trafico.setFecha(getFecha(row.childNode(5).childNode(0).toString().trim()));

			if (trafico.getUnidad().contains("FZO")) {
				trafico.setChofer(
						getChoferId(row.select("a[href]").get(0).attr("href").toString().trim(), cookies, choferes));
			} else {
				trafico.setChofer(0);
			}

			traficoList.add(trafico);
		}

		return traficoList;
	}

	private String getFecha(String cadena) {

		String fecha = "";

		String[] reg = cadena.split("-");

		if (reg.length > 2) {
			fecha = changeDateFormat(reg[2]);
		}

		return fecha;
	}

	private int getChoferId(String path, Map<String, String> cookies, List<Chofer> choferes) {

		Document document = new ConnectURL().getDocument(path, cookies);

		String chofer = document.getElementById("maincontent").select("table.parametros").select("tr>td").get(9)
				.childNode(0).toString();

		int res = new FindChofer().find(chofer, choferes);

		return res;
	}

	private String formatUnidadId(String nUnidad) {

		nUnidad = new StringBuffer(nUnidad).insert(3, "-").toString();

		/*
		 * if(nUnidad.contains("FZO")){ nUnidad = new StringBuffer(nUnidad).insert(3,
		 * "-").toString(); } else { nUnidad = ""; }
		 */

		return nUnidad;
	}

	private int getProductoId(String producto) {

		int res = 0;

		if (producto.contains("MAGNA") || producto.contains("32025")) {
			res = 1;
		} else if (producto.contains("PREMIUM") || producto.contains("32026")) {
			res = 2;
		} else if (producto.contains("DIESEL") || producto.contains("34015")) {
			res = 3;
		}

		return res;
	}

	private Map<String, String> getMapData() {

		Map<String, String> data = new HashMap();

		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		DateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		calendar.add(Calendar.DATE, DAYS_BEFORE_CURRENT);

		Date oldDate = calendar.getTime();

		data.put(INI_DATA, format.format(oldDate));
		data.put(FIN_DATA, format.format(currentDate));

		return data;
	}

	private String changeDateFormat(String date) {

		Date oldDate = null;

		SimpleDateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat writeFormat = new SimpleDateFormat("dd-MM-yyyy");

		try {
			oldDate = readFormat.parse(date.trim());

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return writeFormat.format(oldDate);

	}
}

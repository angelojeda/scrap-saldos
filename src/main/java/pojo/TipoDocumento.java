package pojo;

import java.util.List;

/**
 * Created by Angel on 16/03/2017.
 */

public class TipoDocumento {
	private String tipoDocumento;
	private String totalDocumentos;
	private String importe;
	private String saldo;
	private String interes;
	private String ivaInteres;
	private String total;
	private List<RemisionesProducto> remisionesProductos;
	private String remisionesProductoPath;

	public String getTipoDocumento() {
		return tipoDocumento;
	}

	public void setTipoDocumento(String tipoDocumento) {
		this.tipoDocumento = tipoDocumento;
	}

	public String getTotalDocumentos() {
		return totalDocumentos;
	}

	public void setTotalDocumentos(String totalDocumentos) {
		this.totalDocumentos = totalDocumentos;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getInteres() {
		return interes;
	}

	public void setInteres(String interes) {
		this.interes = interes;
	}

	public String getIvaInteres() {
		return ivaInteres;
	}

	public void setIvaInteres(String ivaInteres) {
		this.ivaInteres = ivaInteres;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public List<RemisionesProducto> getRemisionesProductos() {
		return remisionesProductos;
	}

	public void setRemisionesProductos(List<RemisionesProducto> remisionesProductos) {
		this.remisionesProductos = remisionesProductos;
	}

	public String getRemisionesProductoPath() {
		return remisionesProductoPath;
	}

	public void setRemisionesProductoPath(String remisionesProductoPath) {
		this.remisionesProductoPath = remisionesProductoPath;
	}
}

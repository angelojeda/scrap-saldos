package pojo;

/**
 * Created by Angel on 06/04/2017.
 */

public class Saldo {
	private long id;
	private int gasolinera;
	private String cliente;
	private String encargadoCobro;
	private String formaPago;
	private String estadoSuspencion;
	private long limiteCredito;
	private long disponibleContado;
	private long disponibleCredito;
	private long saldoContado;
	private long comprometidoContado;
	private long moratoriosContado;
	private long totalContado;
	private long comprometidoContadoPemex;
	private long totalContadoPemex;
	private long saldoCredito;
	private long comprometidoCredito;
	private long moratoriosCredito;
	private long totalCredito;
	private String fecha;
	private String orden;

	public long getId() {
		return id;
	}

	public void setId(long id) {
		this.id = id;
	}

	public int getGasolinera() {
		return gasolinera;
	}

	public void setGasolinera(int gasolinera) {
		this.gasolinera = gasolinera;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getEncargadoCobro() {
		return encargadoCobro;
	}

	public void setEncargadoCobro(String encargadoCobro) {
		this.encargadoCobro = encargadoCobro;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getEstadoSuspencion() {
		return estadoSuspencion;
	}

	public void setEstadoSuspencion(String estadoSuspencion) {
		this.estadoSuspencion = estadoSuspencion;
	}

	public long getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(long limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public long getDisponibleContado() {
		return disponibleContado;
	}

	public void setDisponibleContado(long disponibleContado) {
		this.disponibleContado = disponibleContado;
	}

	public long getDisponibleCredito() {
		return disponibleCredito;
	}

	public void setDisponibleCredito(long disponibleCredito) {
		this.disponibleCredito = disponibleCredito;
	}

	public long getSaldoContado() {
		return saldoContado;
	}

	public void setSaldoContado(long saldoContado) {
		this.saldoContado = saldoContado;
	}

	public long getComprometidoContado() {
		return comprometidoContado;
	}

	public void setComprometidoContado(long comprometidoContado) {
		this.comprometidoContado = comprometidoContado;
	}

	public long getMoratoriosContado() {
		return moratoriosContado;
	}

	public void setMoratoriosContado(long moratoriosContado) {
		this.moratoriosContado = moratoriosContado;
	}

	public long getTotalContado() {
		return totalContado;
	}

	public void setTotalContado(long totalContado) {
		this.totalContado = totalContado;
	}

	public long getComprometidoContadoPemex() {
		return comprometidoContadoPemex;
	}

	public void setComprometidoContadoPemex(long comprometidoContadoPemex) {
		this.comprometidoContadoPemex = comprometidoContadoPemex;
	}

	public long getTotalContadoPemex() {
		return totalContadoPemex;
	}

	public void setTotalContadoPemex(long totalContadoPemex) {
		this.totalContadoPemex = totalContadoPemex;
	}

	public long getSaldoCredito() {
		return saldoCredito;
	}

	public void setSaldoCredito(long saldoCredito) {
		this.saldoCredito = saldoCredito;
	}

	public long getComprometidoCredito() {
		return comprometidoCredito;
	}

	public void setComprometidoCredito(long comprometidoCredito) {
		this.comprometidoCredito = comprometidoCredito;
	}

	public long getMoratoriosCredito() {
		return moratoriosCredito;
	}

	public void setMoratoriosCredito(long moratoriosCredito) {
		this.moratoriosCredito = moratoriosCredito;
	}

	public long getTotalCredito() {
		return totalCredito;
	}

	public void setTotalCredito(long totalCredito) {
		this.totalCredito = totalCredito;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getOrden() {
		return orden;
	}

	public void setOrden(String orden) {
		this.orden = orden;
	}
}

package pojo;

/**
 * Created by Angel on 16/03/2017.
 */

public class RemisionesProducto {

	private String numeroDocumentos;
	private String fechaDocumento;
	private String estadoDocumento;
	private String fechaVencimiento;
	private String fechaPagoMoratorio;
	private String diasVencidos;
	private String importe;
	private String saldo;
	private String interes;
	private String ivaInteres;
	private String total;
	private String encargadoCobro;
	private String estadoReclamacion;
	private Remision remision;
	private String remisionPath;

	public String getNumeroDocumentos() {
		return numeroDocumentos;
	}

	public void setNumeroDocumentos(String numeroDocumentos) {
		this.numeroDocumentos = numeroDocumentos;
	}

	public String getFechaDocumento() {
		return fechaDocumento;
	}

	public void setFechaDocumento(String fechaDocumento) {
		this.fechaDocumento = fechaDocumento;
	}

	public String getEstadoDocumento() {
		return estadoDocumento;
	}

	public void setEstadoDocumento(String estadoDocumento) {
		this.estadoDocumento = estadoDocumento;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getFechaPagoMoratorio() {
		return fechaPagoMoratorio;
	}

	public void setFechaPagoMoratorio(String fechaPagoMoratorio) {
		this.fechaPagoMoratorio = fechaPagoMoratorio;
	}

	public String getDiasVencidos() {
		return diasVencidos;
	}

	public void setDiasVencidos(String diasVencidos) {
		this.diasVencidos = diasVencidos;
	}

	public String getImporte() {
		return importe;
	}

	public void setImporte(String importe) {
		this.importe = importe;
	}

	public String getSaldo() {
		return saldo;
	}

	public void setSaldo(String saldo) {
		this.saldo = saldo;
	}

	public String getInteres() {
		return interes;
	}

	public void setInteres(String interes) {
		this.interes = interes;
	}

	public String getIvaInteres() {
		return ivaInteres;
	}

	public void setIvaInteres(String ivaInteres) {
		this.ivaInteres = ivaInteres;
	}

	public String getTotal() {
		return total;
	}

	public void setTotal(String total) {
		this.total = total;
	}

	public String getEncargadoCobro() {
		return encargadoCobro;
	}

	public void setEncargadoCobro(String encargadoCobro) {
		this.encargadoCobro = encargadoCobro;
	}

	public String getEstadoReclamacion() {
		return estadoReclamacion;
	}

	public void setEstadoReclamacion(String estadoReclamacion) {
		this.estadoReclamacion = estadoReclamacion;
	}

	public String getRemisionPath() {
		return remisionPath;
	}

	public void setRemisionPath(String remisionPath) {
		this.remisionPath = remisionPath;
	}

	public Remision getRemision() {
		return remision;
	}

	public void setRemision(Remision remision) {
		this.remision = remision;
	}
}

package update;

import pojo.SaldoAnalitico;
import pojo.Trafico;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by Angel on 19/05/2017.
 */
public class UpdateTrafico {

	private static final Logger logger = Logger.getLogger(UpdateTrafico.class);
	private String traficoURL;
	private int response;

	public void update(List<SaldoAnalitico> saldoAnaliticos) {

		for (SaldoAnalitico saldoAnalitico : saldoAnaliticos) {
			for (Trafico trafico : saldoAnalitico.getTraficos()) {
				response = new Update().send(traficoURL, wsBody(trafico));

				if (response == 201) {
					logger.info("Se actualizaron los datos de trafico - " + saldoAnalitico.getCliente());
				} else {
					logger.error("Error (" + response + ") No se pudo actualizar - " + saldoAnalitico.getCliente());
				}

			}
		}
	}

	private String wsBody(Trafico trafico) {

		String bodyVencimientos = "destino=" + trafico.getEs_gasolinera() + "&origen=" + trafico.getEs_gasolinera()
				+ "&es_gasolinera=" + trafico.getEs_gasolinera() + "&producto=" + trafico.getProducto() + "&unidad="
				+ trafico.getUnidad() + "&cantidad=" + trafico.getCantidad() + "&fecha=" + trafico.getFecha()
				+ "&descargado=" + trafico.getDescargado() + "&temperatura_carga=" + trafico.getTemperatura_carga()
				+ "&manual=" + trafico.getManual() + "&usuario=" + trafico.getUsuario() + "&chofer="
				+ trafico.getChofer();

		return bodyVencimientos;
	}

	public String getTraficoURL() {
		return traficoURL;
	}

	public void setTraficoURL(String traficoURL) {
		this.traficoURL = traficoURL;
	}
}

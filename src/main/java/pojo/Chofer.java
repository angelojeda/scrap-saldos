package pojo;

/**
 * Created by Angel on 11/05/2017.
 */
public class Chofer {
	private int id;
	private String nombre;
	private String apellido;
	private int numEcon;

	public int getId() {
		return id;
	}

	public void setId(int id) {
		this.id = id;
	}

	public String getNombre() {
		return nombre;
	}

	public void setNombre(String nombre) {
		this.nombre = nombre;
	}

	public int getNumEcon() {
		return numEcon;
	}

	public void setNumEcon(int numEcon) {
		this.numEcon = numEcon;
	}

	public String getApellido() {
		return apellido;
	}

	public void setApellido(String apellido) {
		this.apellido = apellido;
	}
}

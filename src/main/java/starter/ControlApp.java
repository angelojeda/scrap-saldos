package starter;

import pojo.Chofer;
import pojo.Estacion;
import pojo.SaldoAnalitico;
import process.Choferes;
import process.ConnectURL;
import process.Estaciones;
import process.ProcessSaldoAnalitico;
import file.DataReader;
import update.Update;
import update.UpdateDetalleEstacion;
import update.UpdateSaldoAnalitico;
import update.UpdateTrafico;
import update.UpdateVencimientos;
import org.apache.log4j.Logger;
import org.springframework.context.ApplicationContext;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angel on 16/05/2017.
 */
public class ControlApp {

	private int refreshTime;

	private static final Logger logger = Logger.getLogger(ControlApp.class);


	public void init(ApplicationContext context) {

		int timeMinutes = convertIntoMinutes(getRefreshTime());

		DataReader estacionesReader = (DataReader) context.getBean("estacionesReader");
		DataReader choferesReader = (DataReader) context.getBean("choferesReader");
		ConnectURL conection = (ConnectURL) context.getBean("conectionUrl");

		UpdateSaldoAnalitico updateSaldoAnalitico = (UpdateSaldoAnalitico) context.getBean("updateSaldoAnalitco");
		UpdateDetalleEstacion updateDetalleEstacion = (UpdateDetalleEstacion) context.getBean("updateDetalleEstacion");
		UpdateTrafico updateTrafico = (UpdateTrafico) context.getBean("updateTrafico");
		UpdateVencimientos updateVencimientos = (UpdateVencimientos) context.getBean("updateVecimiento");

//		List<Chofer> choferes = new Choferes().init(choferesReader.init());
		List<Chofer> choferes = new Choferes().init(listaChofereS());

//		List<Estacion> estaciones = new Estaciones().init(estacionesReader.init());
		List<Estacion> estaciones = new Estaciones().init(listaEstaciones());

		synchronized (this) {
			while (true) {
				try {
					List<SaldoAnalitico> saldosAnaliticos = new ProcessSaldoAnalitico().start(estaciones, choferes);

					updateSaldoAnalitico.update(saldosAnaliticos);
					updateDetalleEstacion.update(saldosAnaliticos);
					updateTrafico.update(saldosAnaliticos);
					updateVencimientos.update(saldosAnaliticos);

					logger.info(
							"Se esperaran " + getRefreshTime() + " minutos para volver a actualizar la informacion");

					this.wait(timeMinutes);
				} catch (InterruptedException e) {
					e.printStackTrace();
				}
			}
		}
	}

	private int convertIntoMinutes(int refreshTime) {

		int result = 60000 * refreshTime;

		return result;
	}

	public int getRefreshTime() {
		return refreshTime;
	}

	public void setRefreshTime(int refreshTime) {
		this.refreshTime = refreshTime;
	}

	private List<String> listaChofereS(){
		 List<String> choferes = new ArrayList();

		choferes.add("1 - Acosta Pierre , 276");
		choferes.add("2 - Daniel Alameda , 617");
		choferes.add("3 - Alvarez Antonio de Jesus , 914");
		choferes.add("4 - Alvarez Luis Anselmo , 523");
		choferes.add("5 - Barraza Francisco , 385");
		choferes.add("6 - Cadena Paul Rene , 905");
		choferes.add("7 - Camacho Antonio , 913");
		choferes.add("8 - Coronado Jorge , 593");
		choferes.add("9 - Cota Hale Javier , 594");
		choferes.add("10 - Gonzalez Adrian , 908");
		choferes.add("11 - Isiordia Juan Gabriel , 500");
		choferes.add("12 - Leon Cristian Alberto , 274");
		choferes.add("13 - Marquez Adolfo , 616");
		choferes.add("14 - Martinez Barrera Joel , 624");
		choferes.add("15 - Mendoza Zapien Victor  , 498");
		choferes.add("16 - Navarro Jorge , 525");
		choferes.add("17 - Nu�ez Lorenzo , 812");
		choferes.add("18 - Ampelio Ulises , 586");
		choferes.add("19 - Perez Exiquio , 904");
		choferes.add("20 - Ramirez Catarino , 618");
		choferes.add("21 - Trasvi�a Alain , 911");
		choferes.add("22 - Vargas Alfonso , 271");

		return choferes;

	}

	private List<String> listaEstaciones(){
		List<String> estaciones = new ArrayList();

		estaciones.add("1 = 0000114432/pin23458 - AUTOSERVICIO PINO PALLAS, SA DE CV  I");
		estaciones.add("2 = 0000111910/col71469 - COMBUSTIBLES Y LUBRICANTES INSURGENTES, SA DE CV");
		estaciones.add("3 = 0000113076/ros17699 - SERVICIO SANTA ROSALIA, SA DE CV");
		estaciones.add("4 = 0000114330/agn44316 - AUTOSERVICIO DE GUERRERO NEGRO, SA DE CV");
		estaciones.add("5 = 0000112395/bou30405 - COMBUSTIBLES BOULEVARD, SA DE CV");
		estaciones.add("6 = 0000110597/qui60008 - SERVICIO QUINAGUI, SA DE CV");
		estaciones.add("7 = 0000114857/con20304 - SERVICIO TRUCK STOP CONSTITUCION, SA DE CV");
		estaciones.add("8 = 0000111129/mat54312 - SERVICIO MATANCITAS, SA DE CV");
		estaciones.add("9 = 0000114933/aab13141 - AUTOSERVICIO BALMACEDA, SA DE CV");
		estaciones.add("10 = 0000115031/bon98653 - AUTOSERVICIO VILLA BONITA, SA DE CV");
		estaciones.add("11 = 0000114789/lib10203 - TRUCK STOP LIBRAMIENTO, SA DE CV");
		estaciones.add("12 = 0000115098/danza201 - DANZANTE, SA DE CV");
		estaciones.add("13 = 0000115246/pin15945 - AUTOSERVICIO PINO PALLAS, SA DE CV  II");
		estaciones.add("14 = 0000115271/viz76764 - SERVICIO VIZCAINO, SA DE CV");
		estaciones.add("15 = 0000106272/asa20145 - SERVICIO SAN ANTONIO, SA DE CV");
		estaciones.add("16 = 0000114063/salm6105 - ABREOJOS");
		estaciones.add("17 = 0000115896/tob76444 - SERVICIO LA TOBA, SA DE CV");
		estaciones.add("18 = 0000116293/squ99102 - SERVICIO QUINAGUI, S.A. DE C.V.");
		estaciones.add("31 = 0000202340/cbs51576 - CBS");

		return estaciones;























	}
}



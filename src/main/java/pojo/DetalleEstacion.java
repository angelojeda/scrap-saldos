package pojo;

/**
 * Created by Angel on 18/04/2017.
 */
public class DetalleEstacion {

	private int estacion;
	private String documentoCargo;
	private Double importeCargo;
	private String formaPago;
	private String moneda;
	private String fechaVencimiento;
	private String tipo;
	private String banco;
	private String documentoPago;
	private String fechaPago;
	private Double importePago;
	private Double interesesMoratorios;
	private Double ivaInteresesMoratorios;

	public int getEstacion() {
		return estacion;
	}

	public void setEstacion(int estacion) {
		this.estacion = estacion;
	}

	public String getDocumentoCargo() {
		return documentoCargo;
	}

	public void setDocumentoCargo(String documentoCargo) {
		this.documentoCargo = documentoCargo;
	}

	public Double getImporteCargo() {
		return importeCargo;
	}

	public void setImporteCargo(Double importeCargo) {
		this.importeCargo = importeCargo;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getFechaVencimiento() {
		return fechaVencimiento;
	}

	public void setFechaVencimiento(String fechaVencimiento) {
		this.fechaVencimiento = fechaVencimiento;
	}

	public String getTipo() {
		return tipo;
	}

	public void setTipo(String tipo) {
		this.tipo = tipo;
	}

	public String getBanco() {
		return banco;
	}

	public void setBanco(String banco) {
		this.banco = banco;
	}

	public String getDocumentoPago() {
		return documentoPago;
	}

	public void setDocumentoPago(String documentoPago) {
		this.documentoPago = documentoPago;
	}

	public String getFechaPago() {
		return fechaPago;
	}

	public void setFechaPago(String fechaPago) {
		this.fechaPago = fechaPago;
	}

	public Double getImportePago() {
		return importePago;
	}

	public void setImportePago(Double importePago) {
		this.importePago = importePago;
	}

	public Double getInteresesMoratorios() {
		return interesesMoratorios;
	}

	public void setInteresesMoratorios(Double interesesMoratorios) {
		this.interesesMoratorios = interesesMoratorios;
	}

	public Double getIvaInteresesMoratorios() {
		return ivaInteresesMoratorios;
	}

	public void setIvaInteresesMoratorios(Double ivaInteresesMoratorios) {
		this.ivaInteresesMoratorios = ivaInteresesMoratorios;
	}
}

package process;

/**
 * Created by Angel on 04/04/2017.
 */
public class ParseCurrencyNumber {

	public Double parse(String number) {

		Double parsedNumber = parseToNumeric(number);

		return parsedNumber;
	}

	private String getRawNumber(String text) {

		String numRaw = text.replace("$", "").replace("PESOS", "").replace(",", "");

		return numRaw;
	}

	private Double parseToNumeric(String number) {

		Double rawNumber = 0.0;
		String numberString = getRawNumber(number);

		if (isNumeric(numberString)) {
			rawNumber = Double.parseDouble(numberString);
		} else {
			// no es un digito
		}

		return rawNumber;
	}

	private boolean isNumeric(String number) {

		boolean numeric = number.matches("-?.*\\d.*");

		return numeric;
	}

}

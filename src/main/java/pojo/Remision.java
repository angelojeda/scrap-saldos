package pojo;

import java.util.List;

/**
 * Created by Angel on 16/03/2017.
 */

public class Remision {
	private String fechaElaboracion;
	private String cliente;
	private String transportista;
	private String tipoDocumentos;
	private String nombreChofer;
	private String numeroRemision;
	private String claveVehiculo;
	private String numeroFactura;
	private String formaPago;
	private String volumenFacturado;
	private String moneda;
	private String producto;
	private String encargadoCobro;
	private String antecedentes;
	private String observaciones;
	private List<Registro> registros;

	public String getFechaElaboracion() {
		return fechaElaboracion;
	}

	public void setFechaElaboracion(String fechaElaboracion) {
		this.fechaElaboracion = fechaElaboracion;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getTransportista() {
		return transportista;
	}

	public void setTransportista(String transportista) {
		this.transportista = transportista;
	}

	public String getTipoDocumentos() {
		return tipoDocumentos;
	}

	public void setTipoDocumentos(String tipoDocumentos) {
		this.tipoDocumentos = tipoDocumentos;
	}

	public String getNombreChofer() {
		return nombreChofer;
	}

	public void setNombreChofer(String nombreChofer) {
		this.nombreChofer = nombreChofer;
	}

	public String getNumeroRemision() {
		return numeroRemision;
	}

	public void setNumeroRemision(String numeroRemision) {
		this.numeroRemision = numeroRemision;
	}

	public String getClaveVehiculo() {
		return claveVehiculo;
	}

	public void setClaveVehiculo(String claveVehiculo) {
		this.claveVehiculo = claveVehiculo;
	}

	public String getNumeroFactura() {
		return numeroFactura;
	}

	public void setNumeroFactura(String numeroFactura) {
		this.numeroFactura = numeroFactura;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getVolumenFacturado() {
		return volumenFacturado;
	}

	public void setVolumenFacturado(String volumenFacturado) {
		this.volumenFacturado = volumenFacturado;
	}

	public String getMoneda() {
		return moneda;
	}

	public void setMoneda(String moneda) {
		this.moneda = moneda;
	}

	public String getProducto() {
		return producto;
	}

	public void setProducto(String producto) {
		this.producto = producto;
	}

	public String getEncargadoCobro() {
		return encargadoCobro;
	}

	public void setEncargadoCobro(String encargadoCobro) {
		this.encargadoCobro = encargadoCobro;
	}

	public String getAntecedentes() {
		return antecedentes;
	}

	public void setAntecedentes(String antecedentes) {
		this.antecedentes = antecedentes;
	}

	public String getObservaciones() {
		return observaciones;
	}

	public void setObservaciones(String observaciones) {
		this.observaciones = observaciones;
	}

	public List<Registro> getRegistros() {
		return registros;
	}

	public void setRegistros(List<Registro> registros) {
		this.registros = registros;
	}
}

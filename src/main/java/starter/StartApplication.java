package starter;

import process.ConnectURL;

import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.log;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class StartApplication {
	
	private static Log logger = LogFactory.getLog("StartApplication");


	public static void main(String[] args) {

		ConnectURL connectURL = new ConnectURL();

//		ApplicationContext context = new ClassPathXmlApplicationContext(args[0]);
		ApplicationContext context = new ClassPathXmlApplicationContext("config/applicationContext.xml");
		
		logger.info("se esta ejecutando");


		ControlApp controlApp = (ControlApp) context.getBean("controlApp");

		controlApp.init(context);

	}
}

package update;

import pojo.RemisionesProducto;
import pojo.SaldoAnalitico;
import java.util.List;
import org.apache.log4j.Logger;

/**
 * Created by Angel on 20/05/2017.
 */
public class UpdateVencimientos {

	private static final Logger logger = Logger.getLogger(UpdateVencimientos.class);
	private String vencimientosURL;
	private int response;

	public void update(List<SaldoAnalitico> saldoAnaliticos) {

		for (SaldoAnalitico saldo : saldoAnaliticos) {
			new Update().clear(vencimientosURL, "estacion=" + saldo.getGasolinera());
			if (!saldo.getSaldoDetallados().get(0).getTipoDocumentos().get(0).getRemisionesProductos().isEmpty()) {
				for (RemisionesProducto saldoVencimiento : saldo.getSaldoDetallados().get(0).getTipoDocumentos().get(0)
						.getRemisionesProductos()) {

					response = new Update().send(vencimientosURL, wsBody(saldoVencimiento, saldo.getGasolinera()));

					if (response == 201) {
						logger.info("Se actualizaron los vencimientos - " + saldo.getCliente());
					} else {
						logger.error("Error (" + response + ") No se pudieron actualizar los vencimientos - "
								+ saldo.getCliente());
					}
				}
			} else
				logger.info("La estacion " + saldo.getCliente() + " no contiene datos de vencimiento");
		}
	}

	private String wsBody(RemisionesProducto saldoVencimiento, int estacion) {

		String bodyVencimientos = "";

		bodyVencimientos = "estacion=" + estacion + "&numero_documento=" + saldoVencimiento.getNumeroDocumentos()
				+ "&fecha_documento=" + saldoVencimiento.getFechaDocumento() + "&estado_documento="
				+ saldoVencimiento.getFechaVencimiento() + "&fecha_vencimiento="
				+ saldoVencimiento.getFechaVencimiento() + "&fecha_pago_moratorio="
				+ saldoVencimiento.getFechaPagoMoratorio() + "&dias_vencidos=" + saldoVencimiento.getDiasVencidos()
				+ "&importe=" + saldoVencimiento.getImporte() + "&saldo=" + saldoVencimiento.getSaldo() + "&interes="
				+ saldoVencimiento.getInteres() + "&iva_interes=" + saldoVencimiento.getIvaInteres() + "&total="
				+ saldoVencimiento.getTotal() + "&encargado_cobro=" + saldoVencimiento.getEncargadoCobro()
				+ "&estado_reclamacion=" + saldoVencimiento.getEstadoReclamacion();

		return bodyVencimientos;
	}

	public String getVencimientosURL() {
		return vencimientosURL;
	}

	public void setVencimientosURL(String vencimientosURL) {
		this.vencimientosURL = vencimientosURL;
	}
}

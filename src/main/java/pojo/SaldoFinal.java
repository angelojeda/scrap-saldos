package pojo;

import java.util.List;

/**
 * Created by Angel on 05/04/2017.
 */

public class SaldoFinal {

	private List<Saldo> saldosAnaliticos;
	private long sumaLimite;
	private long sumaDisponible;
	private long sumaSaldo;
	private long sumaMoratorios;
	private long sumaTotal;

	public long getSumaLimite() {
		return sumaLimite;
	}

	public void setSumaLimite(long sumaLimite) {
		this.sumaLimite = sumaLimite;
	}

	public long getSumaDisponible() {
		return sumaDisponible;
	}

	public void setSumaDisponible(long sumaDisponible) {
		this.sumaDisponible = sumaDisponible;
	}

	public long getSumaSaldo() {
		return sumaSaldo;
	}

	public void setSumaSaldo(long sumaSaldo) {
		this.sumaSaldo = sumaSaldo;
	}

	public long getSumaMoratorios() {
		return sumaMoratorios;
	}

	public void setSumaMoratorios(long sumaMoratorios) {
		this.sumaMoratorios = sumaMoratorios;
	}

	public long getSumaTotal() {
		return sumaTotal;
	}

	public void setSumaTotal(long sumaTotal) {
		this.sumaTotal = sumaTotal;
	}

	public List<Saldo> getSaldosAnaliticos() {
		return saldosAnaliticos;
	}

	public void setSaldosAnaliticos(List<Saldo> saldosAnaliticos) {
		this.saldosAnaliticos = saldosAnaliticos;
	}
}

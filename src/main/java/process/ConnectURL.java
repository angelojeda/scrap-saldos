package process;

import org.apache.log4j.Logger;
import org.jsoup.Connection;
import org.jsoup.HttpStatusException;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Angel on 29/03/2017.
 */
public class ConnectURL {

	private final String ROOT_URL = "https://www.comercialrefinacion.pemex.com";
	private final String INDEX_PATH = "/portal/";
	private final String AGENT_CONEXION = "Mozilla";
	private final String USER_INPUT = "usuario";
	private final String PASS_INPUT = "contrasena";
	private int timeReconect = 10;

	private static final Logger logger = Logger.getLogger(ConnectURL.class);

	public Map<String, String> connect(String user, String pass) {

		synchronized (this) {

			while (login(user, pass).isEmpty()) {
				try {
					logger.info("Se perdio la conexion, intentando conectar en " + timeReconect + " segundos");
					this.wait(timeReconect * 1000);
				} catch (InterruptedException e) {
					logger.error(e);

					e.printStackTrace();
				}
			}
		}
		return login(user, pass);
	}

	public Map<String, String> login(String user, String pass) {

		Map<String, String> cookies = new HashMap();

		try {

			Connection.Response login = Jsoup.connect(ROOT_URL + INDEX_PATH).userAgent(AGENT_CONEXION).timeout(0)
					.data(USER_INPUT, user.trim()).data(PASS_INPUT, pass.trim()).method(Connection.Method.POST)
					.execute();

			cookies = login.cookies();
			if (cookies != null) {
				logger.debug("Se logeo correctamente");
			}

		} catch (HttpStatusException e) {
			logger.error(e);
			return cookies;
		} catch (IOException e) {
			logger.error(e);
			return cookies;
			
		}

		return cookies;
	}

	private Document getDocumentConnect(String path, Map<String, String> cookies) {

		Document document = null;

		while (document == null) {
			try {
				document = Jsoup.connect(ROOT_URL + path).cookies(cookies).timeout(0).get();

			} catch (HttpStatusException e) {
				document = null;
			}

			catch (IOException e) {
				document = null;

				// e.printStackTrace();
			}
		}

		return document;
	}

	public Document getDocument(String path, Map<String, String> cookies) {

		Document d = null;

		if (!(path.isEmpty() && cookies == null)) {

			while (d == null) {
				d = getDocumentConnect(path, cookies);

			}
		}

		return d;
	}

	public Document getDataSubmit(Map<String, String> cookies, Map<String, String> data, String path) {

		Document submit = null;

		try {
			Connection.Response origin = Jsoup.connect(ROOT_URL + path).userAgent(AGENT_CONEXION).timeout(0).data(data)
					.cookies(cookies).method(Connection.Method.POST).execute();

			submit = origin.parse();

		} catch (IOException e) {
			e.printStackTrace();
		}

		return submit;

	}

	public int getTimeReconect() {
		return timeReconect;
	}

	public void setTimeReconect(int timeReconect) {
		this.timeReconect = timeReconect;
	}
}

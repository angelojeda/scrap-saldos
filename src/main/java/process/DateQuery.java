package process;

import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;

/**
 * Created by Angel on 02/05/2017.
 */
public class DateQuery {

	private final String INI_DATA = "fini";
	private final String FIN_DATA = "ffin";
	private final String DATE_FORMAT_PATTERN = "dd/MM/yyyy";
	private final int DAYS_BEFORE_CURRENT = -6;

	private Map<String, String> getDateQuery() {

		Map<String, String> data = new HashMap();

		Calendar calendar = Calendar.getInstance();
		Date currentDate = calendar.getTime();
		DateFormat format = new SimpleDateFormat(DATE_FORMAT_PATTERN);
		calendar.add(Calendar.DATE, DAYS_BEFORE_CURRENT);

		Date oldDate = calendar.getTime();

		data.put(INI_DATA, format.format(oldDate));
		data.put(FIN_DATA, format.format(currentDate));

		return data;
	}
}

package map;

import pojo.SaldoAnalitico;
import pojo.TipoDocumento;
import process.ConnectURL;
import org.jsoup.nodes.Document;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Angel on 23/03/2017.
 */

public class MapTipoDocumento {

	List<TipoDocumento> tipoDocumentos = new ArrayList<TipoDocumento>();

	public SaldoAnalitico mapData(SaldoAnalitico saldo, Map<String, String> cookies) {

		Document document = new ConnectURL().getDocument(saldo.getTipoDocumentoPath(), cookies);

		String remisionesPath = remisionesProductoPath(document);

		TipoDocumento tipoDocumento = new TipoDocumento();
		tipoDocumento.setRemisionesProductoPath(remisionesPath);

		tipoDocumentos.add(tipoDocumento);

		// tipoDocumentos.add(mapDataTable(new TipoDocumento(),document, 14));
		// tipoDocumentos.add(mapDataTable(new TipoDocumento(),document, 16));

		saldo.getSaldoDetallados().get(0).setTipoDocumentos(tipoDocumentos);

		return saldo;
	}

	private TipoDocumento mapDataTable(TipoDocumento tipoDocumento, Document document, int tableNode) {

		tipoDocumento.setTipoDocumento(document.select("table > tbody > tr").get(tableNode).childNode(1).childNode(1)
				.childNode(0).toString().trim());
		tipoDocumento.setTotalDocumentos(
				document.select("table > tbody > tr").get(tableNode).childNode(3).childNode(0).toString().trim());
		tipoDocumento.setImporte(
				document.select("table > tbody > tr").get(tableNode).childNode(5).childNode(0).toString().trim());
		tipoDocumento.setSaldo(
				document.select("table > tbody > tr").get(tableNode).childNode(7).childNode(0).toString().trim());
		tipoDocumento.setInteres(
				document.select("table > tbody > tr").get(tableNode).childNode(9).childNode(0).toString().trim());
		tipoDocumento.setIvaInteres(
				document.select("table > tbody > tr").get(tableNode).childNode(11).childNode(0).toString().trim());
		tipoDocumento.setTotal(
				document.select("table > tbody > tr").get(tableNode).childNode(13).childNode(0).toString().trim());
		tipoDocumento.setRemisionesProductoPath(
				document.select("table > tbody > tr").get(tableNode).select("a[href]").get(0).attr("href").toString());

		return tipoDocumento;
	}

	private String remisionesProductoPath(Document document) {

		String path = "";

		try {
			path = document.select("table > tbody > tr").get(14).select("a[href]").get(0).attr("href").toString();
		} catch (IndexOutOfBoundsException e) {
			path = "";
		}

		return path;
	}
}

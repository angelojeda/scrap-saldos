package process;

import pojo.Estacion;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angel on 22/03/2017.
 */
public class Estaciones {

	private List<Estacion> loginData = new ArrayList();

	public List<Estacion> init(List<String> dataList) {

		for (String data : dataList) {
			loginData.add(getEstaciones(data));
		}
		return loginData;
	}

	private Estacion getEstaciones(String data) {

		Estacion estacion = new Estacion();
		estacion.setIdEstacion(data.split("-")[0].trim().split("=")[0].trim());
		estacion.setNombreEstacion(data.split("-")[1].trim());
		estacion.setUser(data.split("-")[0].trim().split("=")[1].split("/")[0].trim());
		estacion.setPass(data.split("-")[0].trim().split("=")[1].split("/")[1].trim());

		return estacion;
	}

}

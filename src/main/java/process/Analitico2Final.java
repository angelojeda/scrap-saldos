package process;

import pojo.Saldo;
import pojo.SaldoAnalitico;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Angel on 10/04/2017.
 */
public class Analitico2Final {

	public List<Saldo> convert(List<SaldoAnalitico> saldos) {

		List<Saldo> saldosFinales = new ArrayList<Saldo>();

		for (SaldoAnalitico saldo : saldos) {
			saldosFinales.add(parseSaldo(saldo));
		}

		return saldosFinales;
	}

	private Saldo parseSaldo(SaldoAnalitico analitico) {

		Saldo saldo = new Saldo();

		saldo.setOrden("");
		saldo.setFecha(analitico.getFecha());
		saldo.setGasolinera(analitico.getGasolinera());
		saldo.setCliente(analitico.getCliente());
		saldo.setEncargadoCobro(analitico.getEncargadoCobro());
		saldo.setFormaPago(analitico.getFormaPago());
		saldo.setEstadoSuspencion(analitico.getEstadoSuspension());
		saldo.setLimiteCredito(Math.round(analitico.getLimiteCredito()));
		saldo.setDisponibleContado(Math.round(analitico.getSaldoContado()));
		saldo.setDisponibleCredito(Math.round(analitico.getSaldoCredito()));
		saldo.setSaldoContado(Math.round(analitico.getSaldoDetallados().get(0).getSaldoCliente()));
		saldo.setComprometidoContado(Math.round(analitico.getSaldoDetallados().get(0).getComprometidoCliente()));
		saldo.setMoratoriosContado(Math.round(analitico.getSaldoDetallados().get(0).getInteresesMoratoriosCliente()));
		saldo.setTotalContado(Math.round(analitico.getSaldoDetallados().get(0).getTotalCliente()));
		saldo.setComprometidoContadoPemex(Math.round(analitico.getSaldoDetallados().get(0).getComprometidoPemex()));
		saldo.setTotalContadoPemex(Math.round(analitico.getSaldoDetallados().get(0).getTotalPemex()));

		if (analitico.getFormaPago().equals("CREDITO")) {
			saldo.setSaldoCredito(Math.round(analitico.getSaldoDetallados().get(1).getSaldoCliente()));
			saldo.setComprometidoCredito(Math.round(analitico.getSaldoDetallados().get(1).getComprometidoCliente()));
			saldo.setMoratoriosCredito(Math.round(analitico.getSaldoDetallados().get(1).getComprometidoCliente()));
			saldo.setTotalCredito(Math.round(analitico.getSaldoDetallados().get(1).getTotalCliente()));
		}

		return saldo;
	}
}

package map;

import process.ParseCurrencyNumber;
import pojo.SaldoAnalitico;
import org.jsoup.nodes.Element;

/**
 * Created by Angel on 17/03/2017.
 */

public class MapSaldoAnalitico {

	private SaldoAnalitico saldoAnalitico;

	public SaldoAnalitico mapData(SaldoAnalitico saldo, Element element, String date) {
		
//		if(Sald)

		saldo.setFecha(date);
		saldo.setCliente(element.select("td").get(1).childNode(0).toString());
		saldo.setEncargadoCobro(element.select("td").get(3).childNode(0).toString());
		saldo.setFormaPago(element.select("td").get(5).childNode(0).toString());
		saldo.setEstadoSuspension(element.select("td").get(7).childNode(0).toString());
		saldo.setTipoDocumentoPath(element.select("table").select("a[href]").get(0).attr("href").toString());

		if (saldo.getFormaPago().equals("CREDITO")) {
			saldo.setLimiteCredito(
					new ParseCurrencyNumber().parse(element.select("td").get(9).childNode(0).toString()));
			saldo.setSaldoContado(
					new ParseCurrencyNumber().parse(element.select("td").get(11).childNode(0).toString()));
			saldo.setSaldoCredito(
					new ParseCurrencyNumber().parse(element.select("td").get(13).childNode(0).toString()));
		} else {
			saldo.setLimiteCredito(0.0);
			saldo.setSaldoContado(
					new ParseCurrencyNumber().parse(element.select("td").get(10).childNode(0).toString()));
			saldo.setSaldoCredito(0.0);
		}

		return saldo;
	}

}

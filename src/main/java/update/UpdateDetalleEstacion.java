package update;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.List;

import org.apache.log4j.Logger;

import pojo.DetalleEstacion;
import pojo.SaldoAnalitico;

/**
 * Created by Angel on 20/04/2017.
 */
public class UpdateDetalleEstacion {

	private static final Logger logger = Logger.getLogger(UpdateDetalleEstacion.class);
	private String detalleSaldoURL;
	private int response;

	public void update(List<SaldoAnalitico> saldoAnaliticos) {

		for (SaldoAnalitico saldo : saldoAnaliticos) {
			for (DetalleEstacion detalle : saldo.getDetalleEstacionList()) {
				detalle.setEstacion(saldo.getGasolinera());
				response = new Update().send(detalleSaldoURL, wsBody(detalle));

				if (response == 201) {
					logger.info("Se actualizaron los detalles de la estacion " + saldo.getCliente());
				} else {
					logger.error("Error (" + response + ") No se pudo actualizar - " + saldo.getCliente());
				}
			}
		}
	}

	private String wsBody(DetalleEstacion detalle) {
		String body = "estacion=" + detalle.getEstacion() + "&documento_cargo=" + detalle.getDocumentoCargo()
				+ "&importe_cargo=" + detalle.getImporteCargo() + "&forma_pago=" + detalle.getFormaPago() + "&moneda="
				+ detalle.getMoneda() + "&fecha_vencimiento=" + changeDateFormat(detalle.getFechaVencimiento())
				+ "&tipo=" + detalle.getTipo() + "&banco=" + detalle.getBanco() + "&documento_pago="
				+ detalle.getDocumentoPago() + "&fecha_pago=" + changeDateFormat(detalle.getFechaPago())
				+ "&importe_pago=" + detalle.getImportePago() + "&intereses_moratorios="
				+ detalle.getInteresesMoratorios() + "&iva_intereses_moratorios=" + detalle.getIvaInteresesMoratorios();

		return body;
	}

	private String changeDateFormat(String date) {

		Date oldDate = null;

		SimpleDateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			oldDate = readFormat.parse(date.trim());

		} catch (ParseException e) {
			e.printStackTrace();
		}
		return writeFormat.format(oldDate);

	}

	public String getDetalleSaldoURL() {
		return detalleSaldoURL;
	}

	public void setDetalleSaldoURL(String detalleSaldoURL) {
		this.detalleSaldoURL = detalleSaldoURL;
	}
}

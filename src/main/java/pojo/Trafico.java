package pojo;

/**
 * Created by Angel on 22/04/2017.
 */
public class Trafico {

	private int destino;
	private int origen;
	private int es_gasolinera;
	private int producto;
	private String unidad;
	private int cantidad;
	private String fecha;
	private String descargado;
	private Double temperatura_carga;
	private String manual;
	private String usuario;
	private int chofer;

	public int getDestino() {
		return destino;
	}

	public void setDestino(int destino) {
		this.destino = destino;
	}

	public int getOrigen() {
		return origen;
	}

	public void setOrigen(int origen) {
		this.origen = origen;
	}

	public int getEs_gasolinera() {
		return es_gasolinera;
	}

	public void setEs_gasolinera(int es_gasolinera) {
		this.es_gasolinera = es_gasolinera;
	}

	public int getProducto() {
		return producto;
	}

	public void setProducto(int producto) {
		this.producto = producto;
	}

	public String getUnidad() {
		return unidad;
	}

	public void setUnidad(String unidad) {
		this.unidad = unidad;
	}

	public int getCantidad() {
		return cantidad;
	}

	public void setCantidad(int cantidad) {
		this.cantidad = cantidad;
	}

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getDescargado() {
		return descargado;
	}

	public void setDescargado(String descargado) {
		this.descargado = descargado;
	}

	public Double getTemperatura_carga() {
		return temperatura_carga;
	}

	public void setTemperatura_carga(Double temperatura_carga) {
		this.temperatura_carga = temperatura_carga;
	}

	public String getManual() {
		return manual;
	}

	public void setManual(String manual) {
		this.manual = manual;
	}

	public String getUsuario() {
		return usuario;
	}

	public void setUsuario(String usuario) {
		this.usuario = usuario;
	}

	public int getChofer() {
		return chofer;
	}

	public void setChofer(int chofer) {
		this.chofer = chofer;
	}
}

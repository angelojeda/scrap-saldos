package map;

import pojo.RemisionesProducto;
import pojo.SaldoAnalitico;
import process.ConnectURL;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Node;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Map;

/**
 * Created by Angel on 28/03/2017.
 */
public class MapRemisionesProducto {

	List<RemisionesProducto> remisionesProductos = new ArrayList<RemisionesProducto>();

	public SaldoAnalitico mapData(SaldoAnalitico saldo, Map<String, String> cookies) {

		String path = saldo.getSaldoDetallados().get(0).getTipoDocumentos().get(0).getRemisionesProductoPath();
		
		if(!path.isEmpty()) {
			Document document = new ConnectURL().getDocument(path, cookies);

			if (!(document.getElementById("maincontent") == null)) {
				for (Node value : document.getElementById("maincontent").getElementById("maincontent").select("tbody")
						.get(2).childNodes()) {

					if (value.hasAttr("class")) {
						remisionesProductos.add(mapRemisiones(new RemisionesProducto(), value, cookies));
					} else {
						continue;
					}
				}
			}
			
		}


		saldo.getSaldoDetallados().get(0).getTipoDocumentos().get(0).setRemisionesProductos(remisionesProductos);

		return saldo;

	}

	private RemisionesProducto mapRemisiones(RemisionesProducto remisionesProducto, Node value,
			Map<String, String> cookies) {

		remisionesProducto.setRemisionPath(value.childNode(1).childNode(1).attr("href").toString());
		remisionesProducto
				.setNumeroDocumentos(value.childNode(1).childNode(1).childNode(0).toString().replace(" ", ""));
		remisionesProducto.setFechaDocumento(changeDateFormat(value.childNode(3).childNode(0).toString()));
		remisionesProducto.setEstadoDocumento(value.childNode(5).childNode(0).toString());
		remisionesProducto.setFechaVencimiento(changeDateFormat(value.childNode(7).childNode(0).toString()));
		remisionesProducto.setFechaPagoMoratorio(
				value.childNode(9).childNodes().isEmpty() ? "1900-01-01" : value.childNode(9).childNode(0).toString());
		remisionesProducto.setDiasVencidos(value.childNode(11).childNode(0).toString());
		remisionesProducto.setImporte(noFormat(value.childNode(13).childNode(0).toString()));
		remisionesProducto.setSaldo(noFormat(value.childNode(15).childNode(0).toString()));
		remisionesProducto.setInteres(noFormat(value.childNode(17).childNode(0).toString()));
		remisionesProducto.setIvaInteres(noFormat(value.childNode(19).childNode(0).toString()));
		remisionesProducto.setTotal(noFormat(value.childNode(21).childNode(0).toString()));
		remisionesProducto.setEncargadoCobro(value.childNode(23).childNode(0).toString());
		remisionesProducto.setEstadoReclamacion(value.childNode(25).childNode(0).toString());
		// remisionesProducto.setRemision(new MapRemision().mapData(cookies,
		// remisionesProducto.getRemisionPath()) );

		return remisionesProducto;
	}

	private String noFormat(String currencyString) {

		String number = currencyString.replace("$", "").replace(",", "");

		return number;
	}

	private String changeDateFormat(String date) {

		Date oldDate = null;

		SimpleDateFormat readFormat = new SimpleDateFormat("dd/MM/yyyy");
		DateFormat writeFormat = new SimpleDateFormat("yyyy-MM-dd");

		try {
			oldDate = readFormat.parse(date.trim());

		} catch (ParseException e) {
			e.printStackTrace();
		}

		return writeFormat.format(oldDate);
	}
}

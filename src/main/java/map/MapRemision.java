package map;

import process.ConnectURL;
import pojo.Registro;
import pojo.Remision;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.nodes.Node;
import org.jsoup.select.Elements;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

/**
 * Created by Angel on 29/03/2017.
 */
public class MapRemision {

	private Remision remision = new Remision();

	private static final String CONTENIDO_PRINCIPAL = "maincontent";

	private static final String CUERPO_TABLA = "tbody";

	public Remision mapData(Map<String, String> cookies, String remisionPath) {

		Document remisionData = new ConnectURL().getDocument(remisionPath, cookies);

		Elements element = remisionData.getElementById(CONTENIDO_PRINCIPAL).select(CUERPO_TABLA);

		mapRemisionData(element);

		return null;
	}

	private Remision mapRemisionData(Elements elements) {

		Node nodo = elements.get(0);

		remision.setFechaElaboracion(nodo.childNode(1).childNode(1).childNode(0).toString());
		remision.setCliente(nodo.childNode(1).childNode(3).childNode(0).toString());
		remision.setTransportista(nodo.childNode(4).childNode(1).childNode(0).toString());
		remision.setTipoDocumentos(nodo.childNode(4).childNode(3).childNode(0).toString());
		remision.setNombreChofer(nodo.childNode(7).childNode(1).childNode(0).toString());
		remision.setNumeroRemision(nodo.childNode(7).childNode(3).childNode(0).toString());
		remision.setClaveVehiculo(nodo.childNode(10).childNode(1).childNode(0).toString());
		remision.setNumeroFactura(nodo.childNode(10).childNode(3).childNode(0).toString());
		remision.setFormaPago(nodo.childNode(13).childNode(1).childNode(0).toString());
		remision.setVolumenFacturado(nodo.childNode(13).childNode(3).childNode(0).toString());
		remision.setMoneda(nodo.childNode(16).childNode(1).childNode(0).toString());
		remision.setProducto(nodo.childNode(16).childNode(3).childNode(0).toString());
		remision.setEncargadoCobro(nodo.childNode(19).childNode(1).childNode(0).toString());
		remision.setObservaciones(getRemisionObservaciones(elements));
		remision.setRegistros(getRegistros(elements));

		return remision;
	}

	private List<Registro> getRegistros(Elements elementos) {

		List<Registro> registros = new ArrayList<Registro>();

		Registro registro = new Registro();

		for (Element elemento : elementos.get(1).select("tr>td")) {

			if (elemento.attr("align").equals("left")) {
				registro.setConceptoCargo(elemento.childNode(0).toString());

			} else if (elemento.attr("align").equals("right") && (registro.getConceptoCargo() != null)) {
				registro.setImporteUnitario(elemento.childNode(0).toString());
			}
		}

		return null;
	}

	private String getRemisionObservaciones(Elements elementos) {

		String Anotaciones = "";

		for (Element elemento : elementos.get(2).select("tr > td")) {

			if (!(elemento.childNodes().size() == 0)) {

				Anotaciones = Anotaciones + "\n" + elemento.childNode(0).toString();
			}
		}
		return Anotaciones;
	}
}

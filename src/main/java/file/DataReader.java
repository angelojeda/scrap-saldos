package file;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import org.apache.log4j.Logger;

public class DataReader {

	private String filePath;
	private BufferedReader br = null;
	private FileReader fr = null;
	private List<String> dataList = new ArrayList<String>();

	private static Logger logger = Logger.getLogger(DataReader.class);

	public List<String> init() {
		try {
			fr = new FileReader(getClass().getResource(getFilePath()).getFile());
			br = new BufferedReader(fr);

			String sCurrentLine;

			br = new BufferedReader(new FileReader(getClass().getResource(getFilePath()).getFile()));

			while ((sCurrentLine = br.readLine()) != null) {

				if (!(sCurrentLine.isEmpty() || sCurrentLine.startsWith("#"))) {
					dataList.add(sCurrentLine);
//					logger.info("Se encontro : " + sCurrentLine);
				}
				continue;
			}

		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		} catch (NullPointerException e) {
			e.printStackTrace();
		}

		return dataList;
	}

	public String getFilePath() {
		return filePath;
	}

	public void setFilePath(String filePath) {
		this.filePath = filePath;
	}

	public List<String> getDataList() {
		return dataList;
	}

	public void setDataList(List<String> dataList) {
		this.dataList = dataList;
	}

}

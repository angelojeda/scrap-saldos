package pojo;

import java.util.List;

/**
 * Created by Angel on 16/03/2017.
 */

public class SaldoAnalitico {

	private int gasolinera;
	private String fecha;
	private String cliente;
	private String encargadoCobro;
	private String formaPago;
	private String estadoSuspension;
	private Double limiteCredito;
	private Double saldoContado;
	private Double saldoCredito;
	private List<SaldoDetallado> saldoDetallados;
	private String tipoDocumentoPath;
	private List<DetalleEstacion> detalleEstacionList;
	private List<Trafico> traficos;

	public String getFecha() {
		return fecha;
	}

	public void setFecha(String fecha) {
		this.fecha = fecha;
	}

	public String getCliente() {
		return cliente;
	}

	public void setCliente(String cliente) {
		this.cliente = cliente;
	}

	public String getEncargadoCobro() {
		return encargadoCobro;
	}

	public void setEncargadoCobro(String encargadoCobro) {
		this.encargadoCobro = encargadoCobro;
	}

	public String getFormaPago() {
		return formaPago;
	}

	public void setFormaPago(String formaPago) {
		this.formaPago = formaPago;
	}

	public String getEstadoSuspension() {
		return estadoSuspension;
	}

	public void setEstadoSuspension(String estadoSuspension) {
		this.estadoSuspension = estadoSuspension;
	}

	public List<SaldoDetallado> getSaldoDetallados() {
		return saldoDetallados;
	}

	public void setSaldoDetallados(List<SaldoDetallado> saldoDetallados) {
		this.saldoDetallados = saldoDetallados;
	}

	public String getTipoDocumentoPath() {
		return tipoDocumentoPath;
	}

	public void setTipoDocumentoPath(String tipoDocumentoPath) {
		this.tipoDocumentoPath = tipoDocumentoPath;
	}

	public Double getLimiteCredito() {
		return limiteCredito;
	}

	public void setLimiteCredito(Double limiteCredito) {
		this.limiteCredito = limiteCredito;
	}

	public Double getSaldoContado() {
		return saldoContado;
	}

	public void setSaldoContado(Double saldoContado) {
		this.saldoContado = saldoContado;
	}

	public Double getSaldoCredito() {
		return saldoCredito;
	}

	public void setSaldoCredito(Double saldoCredito) {
		this.saldoCredito = saldoCredito;
	}

	public int getGasolinera() {
		return gasolinera;
	}

	public void setGasolinera(int gasolinera) {
		this.gasolinera = gasolinera;
	}

	public List<DetalleEstacion> getDetalleEstacionList() {
		return detalleEstacionList;
	}

	public void setDetalleEstacionList(List<DetalleEstacion> detalleEstacionList) {
		this.detalleEstacionList = detalleEstacionList;
	}

	public List<Trafico> getTraficos() {
		return traficos;
	}

	public void setTraficos(List<Trafico> traficos) {
		this.traficos = traficos;
	}
}
